#include "image.h"
#include "transforms.h"
#include <stdint.h>
#include <stdio.h>

struct image rotate(struct image const img) {
	// input is WxH
	// output is HxW
	struct image rotated = init_image(img.height, img.width);
	
	for( int i = 0; i < img.height; i++ ) {
		
		for( int j = 0; j < img.width; j++ ) {
			
			set_pixel(&rotated, get_pixel(&img, j, img.height - 1 - i), i, j);
			
		}
		
	}
	
	return rotated;
}




