#include "headers.h"
#include "image.h"
#include "padding.h"
#include <stdbool.h>
#include <stdlib.h>

#define FORMAT_TYPE 0x4d42
#define OFFSET_TO_DATA sizeof(struct bmp_header)
#define INFO_HEADER_SIZE 40
#define PLANES_NUM 1
#define BIT_COUNT 24
#define COMPRESSION_TYPE 0
#define COLORS_NUM 0
#define H_RESOLUTION 2835
#define V_RESOLUTION 2835

struct bmp_header create_header(const struct image* img){
	struct bmp_header res = {0};
	//big-endian format
	res.bfType = FORMAT_TYPE; 
	res.bfileSize = sizeof(struct bmp_header) + get_size(img) + padding_size_bytes(img);
	res.bfReserved = 0;
	res.bOffBits = OFFSET_TO_DATA;
	res.biSize = INFO_HEADER_SIZE;
	res.biWidth = img->width;
	res.biHeight = img->height;
	res.biPlanes = PLANES_NUM;
	res.biBitCount = BIT_COUNT;
	res.biCompression = COMPRESSION_TYPE;
	res.biSizeImage = res.bfileSize - res.bOffBits;
	res.biXPelsPerMeter = H_RESOLUTION;
	res.biYPelsPerMeter = V_RESOLUTION;
	res.biClrUsed = COLORS_NUM;
	res.biClrImportant = 0;
    return res;
}

bool header_is_valid(struct bmp_header const * header){
	return ((header->bfType == 0x4D42) 
			&& (header->biHeight > 0) 
			&& (header->biWidth > 0));
}



