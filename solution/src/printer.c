#include "printer.h"
#include "stdio.h"

void print_error(char* msg){
	fprintf(stderr, "%s\n", msg);
}

void print(char* msg){
	fprintf(stdout, "%s\n", msg);
}
