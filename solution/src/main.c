#include "printer.h"
#include "main_logic.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
	
	if (argc != 3){
		
		print_error("Arguments error");
		
		return 1;
	}
	
	return start(argv);
	
}
