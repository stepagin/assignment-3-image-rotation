#include "bmp.h"
#include "headers.h"
#include "image.h"
#include "padding.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


static bool read_pixels(FILE *in, struct bmp_header const *headers, struct image *img){
	*img = init_image(headers->biWidth, headers->biHeight);
	
    if(!img->data) 
		return false;
	
	for (size_t i = 0; i < img->height; i++){
		
        size_t items_num = fread(img->data + img->width*i, sizeof(struct pixel), img->width, in);
        if (items_num!=img->width) 
			return READ_INVALID_SIGNATURE;
		
		int skip_res = fseek(in, calculate_padding(img->width), SEEK_CUR);
        if (skip_res!=0) 
			return READ_INVALID_BITS;
	} 
	
	return READ_OK;	
	
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
	
    if( !in || !img )
        return READ_INVALID_SIGNATURE;
	
    if( fread(&header, sizeof(struct bmp_header), 1, in) != 1 )
        return READ_INVALID_BITS;
	
    if( !header_is_valid(&header) )
        return READ_INVALID_HEADER;
	
    return read_pixels(in, &header, img);
}

static bool write_pixels(FILE* const out, struct image const* img) {
    uint64_t buffer = 0; // pointer to the first object in the array to be written


    for (size_t i = 0; i < img->height; i++){
		
        size_t items_num = fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out);
        if (items_num != img->width) 
			return WRITE_ERROR;
		
        size_t padding_bytes_num = fwrite(&buffer, 1, calculate_padding(img->width), out);
        if (padding_bytes_num != calculate_padding(img->width))
            return WRITE_ERROR;
    }	
	
    return WRITE_OK;
}

enum  write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = {0};
	
    if( !out || !img )
        return WRITE_ERROR;
	
    header = create_header(img);
	
    if( fwrite(&header, sizeof (struct bmp_header), 1, out) != 1 )
        return WRITE_ERROR;
	
    return write_pixels(out, img);
}
