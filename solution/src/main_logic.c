#include "main_logic.h"
#include "bmp.h"
#include "image.h"
#include "printer.h"
#include "transforms.h"
#include <stdbool.h>
#include <stdio.h>

int start(char **argv) {
	FILE* in = NULL;
	FILE* out = NULL;
	struct image in_image = {0};
	struct image out_image = {0};
	
	//const char* const input_file_name = argv[1];
    //const char* const output_file_name = argv[2];

	
	if( open_file(&in, argv[1], "rb") != OPEN_OK ) {
		print_error("Can not open input file");
		
		return 2;
	}
	
	if( from_bmp(in, &in_image) != READ_OK ) {
		print_error("File must be BMP");
		close_file(&in);
		
		return 3;
	}
	
	if( open_file(&out, argv[2], "wb") != OPEN_OK ) {
		print_error("Can not open output file");
		image_free(&in_image);
		close_file(&in);
		
		return 2;
		
	}
			

	
	out_image = rotate(in_image);
	
	if( !out_image.data ) {
		print_error("Rotate error");
		
		image_free(&in_image);
		close_file(&out);
		close_file(&in);
		return 4;
	}
	
	image_free(&in_image);
	
	if( to_bmp(out, &out_image) != WRITE_OK ) {
		
		print_error("Can not write result in file");
		
		image_free(&out_image);
		close_file(&out);
		close_file(&in);
		return 5;
	}
	
	image_free(&out_image);
	close_file(&out);
	close_file(&in);
	printf("Rotated image saved in %s\n", argv[2]);
	return 0;
}
