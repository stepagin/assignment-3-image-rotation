#include "image.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdlib.h>

struct image init_image(size_t width, size_t height) {
	struct image im = {0};
	im.width = width;
	im.height = height;
	im.data = malloc(sizeof(struct pixel) * width * height);
	return im;
}

void image_free(struct image *im) {
	free(im->data);
}

void set_pixel(struct image* img, struct pixel const p, size_t w, size_t h) {
    *(img->data + h * img->width + w) = p;
}

struct pixel get_pixel(struct image const* img, size_t w, size_t h) {
    return *(img->data + h * img->width + w);
}

size_t get_size(struct image const* img) {
    return (sizeof(struct pixel) * img->width * img->height);
}
