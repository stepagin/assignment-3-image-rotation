#include "file.h"
#include <stdbool.h>
#include <stdio.h>

bool open_file(FILE **file, const char *name, const char *mode){
	if (!name)
		return READ_INVALID;
	*file = fopen(name, mode);
	
	if (*file == NULL)
		return READ_INVALID;
	else
		return OPEN_OK;
}

bool close_file(FILE **file){
	if (*file)
		return CLOSE_ERR;
	if (fclose(*file))
		return CLOSE_OK;
	else
		return CLOSE_ERR;
}
