#ifndef TRANSFORMS_H
#define TRANSFORMS_H

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const img );

#endif
