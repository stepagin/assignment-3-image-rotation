#ifndef FILE_H
#define FILE_H

#include <stdbool.h>
#include <stdio.h>


enum open_status  {
  OPEN_OK = 0,
  READ_INVALID
  // коды других ошибок  
  };

bool open_file(FILE **file, const char *name, const char *mode);


enum close_status  {
  CLOSE_OK = 0,
  CLOSE_ERR
  // коды других ошибок
  };

bool close_file(FILE **file);


#endif
