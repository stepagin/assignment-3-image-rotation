#ifndef IMAGE_H
#define IMAGE_H

#include "printer.h"
#include <stdint.h>
#include <stdio.h>

struct image {
  size_t width, height;
  struct pixel *data;
};


struct __attribute__((packed)) pixel {
	uint8_t b, g, r; 
};

struct image init_image(size_t width, size_t height);

void image_free(struct image *im);

void set_pixel(struct image* img, struct pixel p, size_t w, size_t h);

struct pixel get_pixel(struct image const* img, size_t w, size_t h);

size_t get_size(struct image const* img);

#endif
