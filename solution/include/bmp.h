#ifndef BMP_H
#define BMP_H

#include "file.h"
#include "headers.h"
#include "image.h"
#include <stdbool.h>
#include <stdio.h>

 
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  // коды других ошибок  
 };
  
enum read_status from_bmp( FILE* in, struct image* img ); // enum read_status

 
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  // коды других ошибок 
};

enum  write_status to_bmp( FILE* out, struct image const* img ); // enum write_status



#endif
